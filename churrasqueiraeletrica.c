//***********************************************************************//
// Descrição: Churrasqueira Inteligente                                  //
// Objetivo: Desenvolver uma churrasqueira capaz de auxiliar o usuario   //
// no preparo do churrasco, alem de fornecer mecanismos auxiliares e     //
// funcoes automatizadas para o preparo ideal da peça de carne           //
// Projeto da disciplina de Microprocessadores e Microcontroladores      //
// Prof Dr. Roderval Marcelino                                           //
// Autor: Marcelo Seibt de Oliveira                                      //
// Data: 02/12/2016                                                      //
//***********************************************************************//

#include <CIPF.h> //biblioteca que possui definicoes como frequencia de clock utilizada, watchdog, etc
#include <Flex_LCD420.c> //biblioteca para utilizacao do display LCD

//Associação dos pinos as variaveis
#define sn_carvao             PIN_A4 //sensor de nivel do carvao
#define alerta_carvao         PIN_A5 //alerta para falta de carvao, resposta ao sensor de nivel do carvao
#define modomanual            PIN_B0 //interrupcao externa utilizada para alterar o modo de uso do sistema
#define passo_baixo           PIN_B1 //botao que altera a direcao do motor de passo, aproximando o espeto do carvao gradativamente
#define passo_cima            PIN_B2 //botao que altera a direcao do motor de passo, elevando a altura do espeto em relacao ao carvao gradativamente
#define modoautomatico        PIN_B3 //utiliza a interrupcao externa para alterar o modo de utilizacao do sistema
#define lcd_cursor_cima       PIN_B4 //move o cursor do lcd para cima
#define lcd_cursor_baixo      PIN_B5 //move o cursor do lcd para baixo
#define lcd_cursor_ok         PIN_B6 //entrada ENTER do display lcd
#define espeto_giratorio      PIN_B7 //aciona o espeto giratorio
#define girarespetoesq        PIN_C6 //move o espeto para a esquerda
#define girarespetodir        PIN_C7 //move o espeto para a direita
#define churrascopronto       PIN_D3 //aciona o mecanismo auxiliar de alerta para o usuario, indicando fim do preparo do churrasco

//#define WDT_2304MS 
void configuracoes_iniciais(); //funcao que possui as definicoes iniciais e gerais de interrupcoes e do programa como um todo
void tempo_funcionamento(); //funcao que calcula o tempo de execucao atraves da interrupcao do temporizador utilizando o proprio clock interno
void motordc(int1 lado_rot); //funcao motor DC utilizando ponte H para possivelmente (n foi necessario) mudar a direcao do movimento
void motorpasso(int lado_rot); //funcao que controle a utilizacao do motor de passo para regulagem da altura dos espetos
void escalonamento(); //funcao que define o tipo de peça escolhida pelo usuario para o preparo do churrasco
void continua_menu(); //funcao utilizada na tentativa de desafogar a secao da memoria que estava preenchida
void estado_ocioso(); //funcao ou estado ocioso que nada mais e do que um loop que fica aguardando a interacao do usuario atraves das entradas disponiveis
void tipo_carne_picanha(int tarefa); //funcao responsavel por definir as tarefas, tempos e variaveis do preparo da picanha 
void tipo_carne_costela(); //** pode ser implementada posteriormente -> a funcao picanha ja explora o potencial da ideia do projeto
void tipo_carne_maminha(); //**
void tipo_carne_fraldinha(); //**
void tipo_carne_alcatra(); //**
void tipo_carne_file(); //**
void mede_distancia(); //**
void mede_temperatura(); //**
void trata_ext(); //**

byte    num[4] = {0x00,0x01,0x02,0x03}; //pinos utilizados como saida no portC para uso da funcao do stepper   
int8    disp,i, contador=0; //contador variavel global para selecao da carne, alterada na funcao de escalonamento //disp usada na funcao stepper  
unsigned int cont=0; //utilizada para o temporizador
int1    flag=0; //utilizada para o loop da funcao de escalonamento dos tipos de carne, como uma flag de saida
char    c; //variavel utilizada para o display LCD

#int_ext
void trata_ext()
{

   char c;
   //lcd_init(); //inicia o display 
   c='\f'; //limpa o display
   lcd_putc(c);

   printf(LCD_PUTC,"MODO MANUAL"); //imprime no display a frase em questao
   delay_ms(1000);

   c='\n';
   lcd_putc(c);
   
   while( input(modomanual) == 0 ) //permanece aguardando entradas no modo manual enquanto o botao estiver pressionado
   {
      estado_ocioso();
      //restart_wdt();
   }      
}

#int_RTCC   
void tempo_funcionamento() //interrupcao atraves do temporizador que ira ser utilizado para calcular o tempo das tarefas de preparo das carnes
{

   set_timer0(0);
   cont++;
   /*
   if( cont == 2000 )
   {
      tipo_carne_picanha(1);
   }
   */
   
   
   /*
     fout = fclk/(4*prescaler* (256-TMR0)*count) 
//   for prescaler = 1 : 256 and TMR0 = 0 
//   if need 5sec delay 
//   Tout = 1/fout = 1/0.2Hz = 5sec 
//   count = 4Mhz/(4*256*(256-0)*0.2Hz) = 76.29 = 76 counts(approxi) 
*/
   if( cont == 3000 ) //22240 PRATICA  ( para testar configurei menos tempo )
   {
      tipo_carne_picanha(3);
   }
     


   /*
   cont++;
   
   if( (contador == 0) && (cont == 468750) ) //se o contador=0 ou seja, a carne for picanha, e apos 2 minutos, chama a funcao com o parametro referente a tarefa desejada
   {
      tipo_carne_picanha(1);
   }
   
   if( (contador == 0) && (cont == 3515625) )  //se o contador=0 ou seja, a carne for picanha, e apos 15 minutos, chama a funcao com o parametro referente a tarefa desejada
   {
      tipo_carne_picanha(2);
   }
   
   if( (contador == 0) && (cont == 4687500) ) //se o contador=0 ou seja, a carne for picanha, e apos 20 minutos, chama a funcao com o parametro referente a tarefa desejada
   {
      tipo_carne_picanha(3);
   }
   // Estouro desejado do contador = tempodesejado (s) / ( 256 * 10^-6 ) 
   // 1 seg = 3906
   // 1 minuto = 234375
   */
   

}

void configuracoes_iniciais() //ativa as interrupcoes e define configuracoes gerais para o sistema
{

   enable_interrupts(global | int_ext); //ativa interrupcao global e interrupcao externa atraves do pino b0 do pic16f877A
   //enable_interrupts(INT_RTCC); //ativa interrupcao  
   //setup_ADC_ports(AN0);
   setup_adc_ports(AN0_AN1_AN3); //Configuração dos pinos setados para utilizacao do conversor AD 
   setup_adc(ADC_CLOCK_INTERNAL); //Configuração do clock do conversor AD 
   //setup_timer_0(RTCC_INTERNAL|RTCC_DIV_4); //define o tipo de interrupcao utilizada no timer e o prescaler utilizado 
   //setup_wdt(wdt_ON);
   //setup_oscillator(OSC_4MHZ);
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_256|RTCC_8_BIT);
   enable_interrupts(INT_TIMER0); 
     
   set_tris_b(0xFF);                    //configura o port B como entrada   
   set_tris_c(0x00000000);              //configura o port C como saida
   
   //ajustar altura espeto padrao
   output_low(girarespetoesq); // ** inicia os motores desligados
   output_low(girarespetodir); 
   
   motorpasso(0); //desloca o espeto ate a altura iniciado desejada para o ajuste posterior correto do sistema
   delay_ms(1370); //tempo ficticio necessario para o deslocamento do espeto ate a altura desejada
   motorpasso(2); //desliga o motor de passo
   
   if( input(sn_carvao) == 0 ) //verifica se o repositor de carvao possui carvao acima do nivel desejado
   {
      output_high(alerta_carvao); //liga o alerta
      //restart_wdt();
   }

}

void mede_temperatura() //funcao responsavel para simular a medicao da temperatura, realizar a conversao para graus celsius Cº e coloca-la no display
{

    set_adc_channel(0); //configura o pic para ler os dados do AN0
    long int valor;
    float temp;
     
    valor = read_adc(); //leitura do valor analogico de entrada no pino AN0 do pic16f877A
    valor = valor * 500; //formula utilizada na conversao
    temp  = valor/1023;  //formula utilizada na conversao
    
    c='\f';
    lcd_putc(c);
    printf(LCD_PUTC,"Temperatura = %1f graus",temp); 
    delay_ms(1000);

}

void mede_distancia() //funcao responsavel para simular a medicao da distancia, realizar a conversao para cm e coloca-la no display
{

   set_adc_channel(1); //configura o pic para ler os dados do AN1
   long int valor;
   float distancia;
   
   valor = read_adc(); //leitura do valor analogico de entrada no pino AN1 do pic16f877A
   distancia = -0.03793 * valor + 27.2131; //formula utilizada na conversao
   
    c='\f';
    lcd_putc(c);
    printf(LCD_PUTC,"Distancia = %1f cm",distancia);
    delay_ms(2000);   

}

void motorpasso(int lado_rot) //funcao que define a direcao e produz o movimento do motor de passo, utilizada para ajustar a altura do espeto
{

    if( lado_rot == 0 )
    {
          for(i=0; i<=3;i++) // move p frente
          {
             disp = num[i];
             output_c(disp);
             delay_ms(200);
          }  
    }else{
       if( lado_rot == 1 )
       {
          for(i=3;i>0;i--) // move p tras
          {
             disp = num[i];
             output_c(disp);
             delay_ms(200);
          } 
       }else{
          if( lado_rot == 2 ) // permanece   
          {
            disp = 0;
            output_c(disp);
            delay_ms(50);
          }  
       }
    }

}

void motordc(int1 lado_rot) //funcao que define a direcao e produz o movimento do motor dc, utilizada para acionar o movimento do espeto giratorio
{                          //Poderia ter utilizado transistores ao inves da ponte H mas o projeto ja foi feito pensando futuramente, em adicionar outras funcoes que utilizem

   if( lado_rot == 0 ) // 0 = esquerda
   {
      output_high(girarespetodir);
      output_low(girarespetoesq);
   }
   if( lado_rot == 1 )
   {
      output_low(girarespetoesq);
      output_high(girarespetodir);
   }

}

void escalonamento() //define a peça de carne escolhida pelo usuario para o preparo
{

   char c;
   lcd_init();

   c='\f';
   lcd_putc(c);
     
   do
   {
      if( contador == 0 ) //caso contador = 0 , carne escolhida é a picanha, entao executa as funcoes e procedimento correspondentes ao preparo da picanha
      {
      
         do
         {
               printf(LCD_PUTC,"-> 1 - PICANHA");
               c='\n';
               lcd_putc(c);
               printf(LCD_PUTC,"   2 - COSTELA"); 
               c='\n';
               lcd_putc(c);
               printf(LCD_PUTC,"   3 - MAMINHA");
               c='\n';
               lcd_putc(c);
               printf(LCD_PUTC,"   4 - FILE");
               while(input(lcd_cursor_baixo) == 1 && input(lcd_cursor_ok) == 1)
               {
                  delay_ms(1000);
                  //restart_wdt();
               }
         }while ( input(lcd_cursor_baixo) == 1 && input(lcd_cursor_ok) == 1 );
               c='\f';
               lcd_putc(c);
               delay_ms(2000);
               if( input(lcd_cursor_ok) == 0 ) //caso o botao enter esteja pressionado
               {
                  c='\f';
                  lcd_putc(c);
                  tipo_carne_picanha(0); //chama a funcao da picanha passando como parametro a tarefa inicial 
               }else{
                     if( input(lcd_cursor_baixo) == 0 ) //caso o botao para baixo esteja pressionado incrementa o contador para seguir na proxima tela do menu
                     {
                        contador++;
                     }
               }
     }
     if( contador == 1 ) //caso contador = 1 , carne escolhida é a costela, entao executa as funcoes e procedimento correspondentes ao preparo da costela
     {
         do
         {
               c='\f';
               lcd_putc(c);
               printf(LCD_PUTC,"   1 - PICANHA");
               c='\n';
               lcd_putc(c);
               printf(LCD_PUTC,"-> 2 - COSTELA"); 
               c='\n';
               lcd_putc(c);
               printf(LCD_PUTC,"   3 - MAMINHA");
               c='\n';
               lcd_putc(c);
               printf(LCD_PUTC,"   4 - FILE");
         while(input(lcd_cursor_baixo) == 1 && input(lcd_cursor_ok) == 1 && input(lcd_cursor_cima) == 1)
               {
                  delay_ms(1000);
                  //restart_wdt();
               }
         }while ( input(lcd_cursor_baixo) == 1 && input(lcd_cursor_ok) == 1 && input(lcd_cursor_cima) == 1 );
               delay_ms(2000);
               if( input(lcd_cursor_ok) == 0 )
               {
                  c='\f';
                  lcd_putc(c);
                  tipo_carne_costela(); // como a ideia é simular a idealizacao do projeto e a funcao picanha ja utiliza as funcoes desejadas, preferi deixar p futuramente
               }else{                   // implementar o preparo dos outros tipos de carne
                     if( input(lcd_cursor_baixo) == 0 )   
                     {
                        contador++;
                        c='\f';
                        lcd_putc(c);
                     }else{
                       if( input(lcd_cursor_cima) == 0 )   
                       {
                           contador--;
                           c='\f';
                           lcd_putc(c);
                       }
                     }
                   }
     }
     
     if( contador == 2 ) //caso contador = 1 , carne escolhida é a maminha, entao executa as funcoes e procedimento correspondentes ao preparo da maminha
     {
        do{
            c='\f';
            lcd_putc(c);
            printf(LCD_PUTC,"   1 - PICANHA");
            c='\n';
            lcd_putc(c);
            printf(LCD_PUTC,"   2 - COSTELA");
            c='\n';
            lcd_putc(c);
            printf(LCD_PUTC,"-> 3 - MAMINHA");
            c='\n';
            lcd_putc(c);
            printf(LCD_PUTC,"   4 - FILE");
        while(input(lcd_cursor_baixo) == 1 && input(lcd_cursor_ok) == 1 && input(lcd_cursor_cima) == 1)
               {
                  delay_ms(1000);
                  //restart_wdt();
               }
        }while( input(lcd_cursor_ok) == 1 && input(lcd_cursor_baixo) == 1 && input(lcd_cursor_cima) == 1 );
               delay_ms(2000);
               if( input(lcd_cursor_ok) == 0 )
               {
                  c='\f';
                  lcd_putc(c);
                  tipo_carne_maminha(); // como a ideia é simular a idealizacao do projeto e a funcao picanha ja utiliza as funcoes desejadas, preferi deixar p futuramente
               }else{                   // implementar o preparo dos outros tipos de peças de carne
                     if( input(lcd_cursor_baixo) == 0 )   
                     {
                        contador++;
                        c='\f';
                        lcd_putc(c);
                     }else{
                        if( input(lcd_cursor_cima) == 0 )   
                        {
                           contador--;
                           c='\f';
                           lcd_putc(c);
                        }
                    }
                }     
     }
      if( contador == 3 )
      {
         do
         {
            c='\f';
            lcd_putc(c);
            printf(LCD_PUTC,"   1 - PICANHA");
            c='\n';
            lcd_putc(c);
            printf(LCD_PUTC,"   2 - COSTELA");
            c='\n';
            lcd_putc(c);
            printf(LCD_PUTC,"   3 - MAMINHA");
            c='\n';
            lcd_putc(c);
            printf(LCD_PUTC,"-> 4 - FILE");
         while(input(lcd_cursor_baixo) == 1 && input(lcd_cursor_ok) == 1 && input(lcd_cursor_cima) == 1)
               {
                  delay_ms(1000);
                  //restart_wdt();
               }
               //restart_wdt();
      }while( input(lcd_cursor_ok) == 1 && input(lcd_cursor_baixo) == 1 && input(lcd_cursor_cima) == 1 );
               delay_ms(2000);
               if( input(lcd_cursor_ok) == 0 )
               {
                  c='\f';
                  lcd_putc(c);
                  tipo_carne_file(); // como a ideia é simular a idealizacao do projeto e a funcao picanha ja utiliza as funcoes desejadas, preferi deixar p futuramente
               }else{                // implementar o preparo dos outros tipos de peças de peças de carne
                     if( input(lcd_cursor_baixo) == 0 )   
                     {
                        contador++;
                        c='\f';
                        lcd_putc(c);
                     }else{
                        if( input(lcd_cursor_cima) == 0 )
                        {
                           contador--;
                           c='\f';
                           lcd_putc(c);
                        }
                    }
               }
      }
      
  continua_menu();
  }while( flag == 0 );
  contador = 0;
  flag = 0;

}

void continua_menu() //continua o menu, dividi em duas funcoes na tentativa de desafogar setor especifico da memoria RAM..
{
  
      if( contador == 4 )
      {
         
          do
          {
            c='\f';
            lcd_putc(c);
            printf(LCD_PUTC,"-> 5 - FRALDINHA");
            c='\n';
            lcd_putc(c);
            printf(LCD_PUTC,"   6 - ALCATRA");
            c='\n';
            while(input(lcd_cursor_baixo) == 1 && input(lcd_cursor_ok) == 1 && input(lcd_cursor_cima) == 1)
            {
               delay_ms(1000);
               //restart_wdt();
            }
            //restart_wdt();
          }while( input(lcd_cursor_ok) == 1 && input(lcd_cursor_baixo) == 1 && input(lcd_cursor_cima) == 1 );
               delay_ms(2000);
               if( input(lcd_cursor_ok) == 0 )
               {
                  c='\f';
                  lcd_putc(c);
                  tipo_carne_fraldinha();
               }else{
                     if( input(lcd_cursor_baixo) == 0 )   
                     {
                        contador++;
                        c='\f';
                        lcd_putc(c);
                     }else{
                        if( input(lcd_cursor_cima) == 0 )
                        {
                           contador--;
                           c='\f';
                           lcd_putc(c);
                        }
                    }
                }
      }
      
}

void tipo_carne_picanha(int tarefa) //funcao responsavel por gerenciar o preparo da picanha, estruturando as tarefas e procedimentos para tal
{

   c='\f';
   lcd_putc(c);
   printf(LCD_PUTC,"PECA: PICANHA \n");
   delay_ms(500);
   int8 delay1=0; //delay2=0, delay3=0;
   
   if( tarefa == 0 )
   {
      printf(LCD_PUTC,"AUMENTANDO 20cm..");
      delay_ms(1000);
      while(delay1 < 5)
      {
         for(i=0; i<=3;i++) // move p frente
         {
             disp = num[i];
             output_c(disp);
             delay_ms(1000);
             delay1++;
         }
      }
   }
 /*
   if( tarefa == 1 ) //funcoes comentadas por preenchimento do setor da memoria RAM.. 
   {                 
      printf(LCD_PUTC,"AUMENTANDO 20cm..");
      while(delay2 < 3)
      {
         for(i=0; i<=3;i++) // move p frente
         {
             disp = num[i];
             output_c(disp);
             delay_ms(1000);
             delay2++;
         }
      }
        
   
   }
   */
   
   /*
   if( tarefa == 2 )
   {
      printf(LCD_PUTC,"ABAIXANDO 20cm..");
      
      while(delay3 < 5)
      {
         for(i=3;i>0;i--) // move p tras
         {
             disp = num[i];
             output_c(disp);
             delay_ms(1000);
         } 
      }
      
   }
   */
   if( tarefa == 3 ) //tarefa final do preparo da carne, avisar que o churrasco está pronto
   {
      while(true)
      {
         printf(LCD_PUTC,"INTERRUPCAO");
         c='\n';
         lcd_putc(c);
         printf(LCD_PUTC,"CHURRASCO ESTA PRONTO");
         delay_ms(2000);
         output_high(churrascopronto);
      }
   }
 
   estado_ocioso(); //funcao que que fica na "escuta" ao usuario p alteracao das entradas automaticamente definidas pelo sistema, caso este queira altera-las

}

void tipo_carne_maminha() //funcao a principio nao implementada pois n ha necessidade a fins de explorar a potencialidade da ideia ja que a funcao da picanha ja tem essa finalidade
{
   printf(LCD_PUTC," MAMINHA ");
}

void tipo_carne_costela() //funcao a principio nao implementada pois n ha necessidade a fins de explorar a potencialidade da ideia ja que a funcao da picanha ja tem essa finalidade
{
   printf(LCD_PUTC," COSTELA ");
}

void tipo_carne_file() //funcao a principio nao implementada pois n ha necessidade a fins de explorar a potencialidade da ideia ja que a funcao da picanha ja tem essa finalidade
{
   printf(LCD_PUTC," FILE ");
}

void tipo_carne_fraldinha() //funcao a principio nao implementada pois n ha necessidade a fins de explorar a potencialidade da ideia ja que a funcao da picanha ja tem essa finalidade
{
   printf(LCD_PUTC," FRALDINHA ");
}

void estado_ocioso() //funcao que que fica na "escuta" ao usuario p alteracao das entradas automaticamente definidas pelo sistema, caso este queira altera-las
{
 
    while(true)
    {
         //ENTRADA ALTURA DO ESP
         if( input(passo_cima) == 1 && input(passo_baixo) == 0 )
         {
            do{
                 motorpasso(0); 
                 //restart_wdt();
                 
              }while( input(passo_cima) == 1 && input(passo_baixo) == 0 );  
         
         }
         if( input(passo_cima) == 0 && input(passo_baixo) == 1 )
         {
           do{
                 motorpasso(1);
                 //restart_wdt();
             }while( input(passo_cima) == 0 && input(passo_baixo) == 1 );                    
         }
         if( input(passo_cima) == 1 && input(passo_baixo) == 1 )
         {
           do{
                 motorpasso(2);
                 //restart_wdt();
             }while(input(passo_cima) == 1 && input(passo_baixo) == 1 );
         }

         if( input(espeto_giratorio) == 0 )
         {
            output_high(girarespetodir);
         }
         if( input(espeto_giratorio) == 1 )
         {
            output_low(girarespetodir);
         }
         mede_temperatura();
         mede_distancia();
    }
}

void main() 
{
    
   configuracoes_iniciais(); //define as definicoes do projeto, parametros iniciais e ativa as interrupcoes utilizadas
   
   while(TRUE)
   {
      char c;
      lcd_init();
      
       printf(LCD_PUTC,"CHURR INTELIGENTE");
       c='\n';
       lcd_putc(c);
       delay_ms(200);
       printf(LCD_PUTC,"INTELIGENTE");
       c='\n';
       lcd_putc(c);
       delay_ms(200);
       printf(LCD_PUTC,"ESCOLHA O MODO:");
       c='\n';
       lcd_putc(c);
       delay_ms(200);
       printf(LCD_PUTC,"MANUAL/AUTOMATICO");
       delay_ms(200);
       
       while( input(modoautomatico) == 1 ) //aguarda o usuario interagir com o sistema
       {
            c='\f';
            lcd_putc(c);  
            printf(LCD_PUTC,"AGUARDANDO...");
            delay_ms(500);
            //restart_wdt();
       }
       escalonamento(); //chama a funcao para selecionar qual carne ira desejar assar no churrasco
       //restart_wdt();
   }
   
}
